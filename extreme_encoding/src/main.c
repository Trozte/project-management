#include <stdio.h>

int main(void)
{
    int T = 0;
    int n = 0;
    int a[10000];
    int b[10000];

    scanf("%d", &T); /* Number of test cases */

    for (int i = 0; i < T; i++) {
        scanf("%d", &n); /* Number of the array */

        for (int j = 0; j < n; j++) {
            scanf("%d", &a[j]); /* Encoded value */
            b[j] = a[j] >> 16;  /* Value in array b */
            a[j] = a[j] - (b[j] << 16); /* Value in array a */
        }

        /* Print Output */
        
        printf("Case %d:\n", i + 1);

        for (int j = 0; j < n; j++) {
            printf("%d ", a[j]);
        }

        printf("\n");
        for (int j = 0; j < n; j++)
        {
            printf("%d ", b[j]);
        }

        printf("\n");
    }

    return 0;
}