# Project Management Dynamic #

### CodeChef Challenges to solve: ###

* Packaging Cupcakes
* Chef Under Pressure
* Extreme Encoding

### Team members ###

* Rodrigo Reyes (Project Manager)
* Juan Pablo Iñigo (Programmer)
* Eduardo Tostado (Programmer)

### Trello ###

https://trello.com/b/eCNOCPhv