/*-------------------------------------------------------
 * Actividad de Administración de Proyectos: Challenge 1 - Packaging Cupcakes
 * Fecha: 21-02-2017
 * Autor:
 *      A01064215 Rodrigo Reyes Murillo
 */
#include <stdio.h>
#include <stdlib.h>

int main()
{
    int t, n, i, result;
    scanf("%d", &t);
    for (i = 0; i < t; i++) {
        scanf("%d", &n);
		if (n%2 == 0)
			result = (n/2) + 1;
		else
			result = (n+1)/2;
        printf("%d\n", result);
    }
    return 0;
}